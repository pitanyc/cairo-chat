# cairo-chat

This is a Javascript based web chat application.  The features include smileys, sounds, custom buddy icons, languagues, automatic translation of chats, offline messages, blinkers, and many more.

## Creator

[Peter Szocs](http://peter.szocs.info/), Senior Software Developer at [Bloomberg LP](http://www.bloomberg.com/).

* https://twitter.com/szocspeter
* https://www.facebook.com/szocs.peter
* https://www.linkedin.com/in/szocspeter
* https://github.com/gepesz
